import terminal
import times
import strformat

export ForegroundColor
export BackgroundColor
export Style

type
  Appearance* = tuple
    foreg: ForegroundColor
    backg: BackgroundColor
    nobg: bool
    style: Style
  
  theme_t* = tuple
    info: Appearance
    warning: Appearance
    error: Appearance
    message: Appearance
    trace: Appearance
    fatal: Appearance
    log: Appearance
  
  logger_t* = tuple
    provider: string
    minlevel: int
    colors: bool
    disk: bool
    logfile: string
    theme: theme_t

const
  defaultTheme*: theme_t = (
    info: (fgCyan, bgBlack, true, styleBright),
    warning: (fgYellow, bgBlack, true, styleBright),
    error: (fgRed, bgBlack, true, styleBright),
    message: (fgWhite, bgBlack, true, styleBright),
    trace: (fgCyan, bgBlack, true, styleDim),
    fatal: (fgBlack, bgRed, false, styleBright),
    log: (fgWhite, bgBlack, true, styleDim))


proc init_logger*(provider: string, minlevel: int, fileName: string, theme: theme_t = defaultTheme): logger_t =
  ## Initialize a logger that logs to a file
  result = (provider, minlevel, true, true, fileName, theme)

proc init_loggernc*(provider: string, minlevel: int, fileName: string): logger_t =
  ## Initialize a logger that logs to a file but does not use colors in console
  result = (provider, minlevel, false, true, filename, defaultTheme)

proc init_logger*(provider: string, minlevel: int, theme: theme_t = defaultTheme): logger_t =
  ## Initialize a logger that does not log to a file
  result = (provider, minlevel, true, false, "", theme)

proc init_loggernc*(provider: string, minlevel: int): logger_t =
  ## Initialize a logger that does not log to a file or use colors in console
  result = (provider, minlevel, false, false, "", defaultTheme)



proc println(logger: logger_t, ap: Appearance, level: int, header: string, message: string) =
  proc time_stamp(): string =
    result = "[" & getClockStr() & "]"

  if level < logger.minlevel: return
  if not logger.colors:
    echo(fmt"{time_stamp()} - [{header}] {message}")
    return

  if ap.noBg:
    styledEcho(time_stamp(), " - [", ap.foreg, ap.style, header, resetStyle, "] ", message)
  else:
    styledEcho(time_stamp(), " - [", ap.foreg, ap.backg, ap.style, header, resetStyle, "] ", message)

proc info*(logger: logger_t, level: int, message: string) =
  println(logger, logger.theme.info, level, "info", message)

proc message*(logger: logger_t, level: int, message: string) =
  println(logger, logger.theme.message, level, "message", message)

proc trace*(logger: logger_t, level: int, message: string) =
  println(logger, logger.theme.trace, level, "trace", message)

proc warning*(logger: logger_t, level: int, message: string) =
  println(logger, logger.theme.warning, level, "warning", message)

proc fatal*(logger: logger_t, level: int, message: string) =
  println(logger, logger.theme.fatal, level, "fatal", message)

proc error*(logger: logger_t, level: int, message: string) =
  println(logger, logger.theme.error, level, "error", message)

proc log*(logger: logger_t, level: int, message: string) =
  println(logger, logger.theme.log, level, "log", message)



