import nake
import strformat
import os

when defined(linux):
  const suffix_static = ".a"
  const suffix_dynamic = ".so"
  const prefix = "lib"

elif defined(windows):
  const suffix_static = ".lib"
  const suffix_dynamic = ".dll"
  const prefix = ""

elif defined(osx):
  const suffix_static = ".a"
  const suffix_dynamic = ".dynlib"
  const prefix = "lib"

else:
  echo "Platform is not supported by the build file, please build manually"
  quit 1


const
  name = "consoleutil"
  src = "src/log.nim"
  rebuild_on = @[src]
  cache = ".nim"
  version = "1.0.0"
  examples = @[
    "examples/basic.nim"
  ]

  bin_name = joinPath("dist/lib", fmt"{prefix}{name}")

task "static", "Build a static library":
  createDir("dist/lib")
  if name.needsRefresh(rebuild_on):
    direShell(nimExe, "c", "--app:staticlib", fmt"--out:{bin_name}{suffix_static}", fmt"--nimcache:{cache}", src)

task "dynamic", "Build as a dynamic library":
  createDir("dist/lib")
  if name.needsRefresh(rebuild_on):
     direShell(nimExe, "c", "--app:lib", fmt"--out:{bin_name}{suffix_dynamic}", fmt"--nimcache:{cache}", src)   

task "all", "Build both the static and dynamic library":
  runTask("static")
  runTask("dynamic")

task "clean", "Clean all generated build-files":
  if cache.dirExists():
    removeDir(cache)

  removeDir("dist")

  
task "dist-clean", "Clean everything. Yes":
  runTask("clean")

#  if "release".dirExists():
#    removeDir("release")

#  if "dist".dirExists():
#    removeDir("dist")

  if "nakefile".fileExists():
    removeFile("nakefile")

  if "nimcache".dirExists():
    removeDir("nimcache")

task "examples", "Build examples":
  createDir("dist/bin/examples")
  echo "Building examples..."
  for item in examples:
    let bin = joinPath("dist/bin/examples", item.splitFile().name)
    echo "\t", bin
    direShell(nimExe, "c", fmt"--out:{bin}", fmt"--nimcache:{cache}", item)   

task "package", "Build a release package":
  runTask("dist-clean")
  runTask("all")
  runTask("examples")
  copyDir(joinPath("dist", "bin"), "release")
  copyDir(joinPath("dist", "lib"), "release")
  copyDir("src", joinPath("release", "include"))
#  copyFile("README.md", joinPath("release", "doc", "README.md"))
  direShell("tar", "-c", "-z", "-f", fmt"{name}-{version}.tgz", "release")
  removeDir("release")
  echo fmt"Created distribution package {name}-{version}.tgz"

# Linux-specific tasks
#when defined(linux):
#  task "install", "Install program for system-wide use":
#    discard # TODO: Implement
