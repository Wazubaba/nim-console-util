# Package

version       = "0.1.0"
author        = "Wazubaba"
description   = "Simple utilities for working with the console"
license       = "LGPLv3"
srcDir        = "src"

# Dependencies

requires "nim >= 0.18.1"
requires "nake >= 1.9.2"

